import { generateContrastColors } from '../../helpers/color';

import { Crosshair, CrosshairPriceAndCoordinate } from '../../model/crosshair';
import { PriceScale } from '../../model/price-scale';
import { Series } from '../../model/series';
import { PriceAxisViewRendererCommonData, PriceAxisViewRendererData } from '../../renderers/iprice-axis-view-renderer';

import { PriceAxisView } from './price-axis-view';

export type CrosshairPriceAxisViewValueProvider = (priceScale: PriceScale) => CrosshairPriceAndCoordinate;

export class CrosshairPriceAxisView extends PriceAxisView {
	private _source: Crosshair;
	private readonly _priceScale: PriceScale;
	private readonly _valueProvider: CrosshairPriceAxisViewValueProvider;

	public constructor(source: Crosshair, priceScale: PriceScale, valueProvider: CrosshairPriceAxisViewValueProvider) {
		super();
		this._source = source;
		this._priceScale = priceScale;
		this._valueProvider = valueProvider;
	}

	protected _updateRendererData(
		axisRendererData: PriceAxisViewRendererData,
		paneRendererData: PriceAxisViewRendererData,
		commonRendererData: PriceAxisViewRendererCommonData
	): void {
		axisRendererData.visible = false;
		const options = this._source.options().horzLine;
		if (!options.labelVisible) {
			return;
		}

		const firstValue = this._priceScale.firstValue();
		if (!this._source.visible() || this._priceScale.isEmpty() || (firstValue === null)) {
			return;
		}

		const value = this._valueProvider(this._priceScale);
		/**
		 * 找到当前 crosshair.marker 所在那条线
		 */
		const serieses = this._source.serieses();
		const timePointIndex = this._source.appliedIndex();
		const series = serieses.find((s: Series) => s.markerDataAtIndex(timePointIndex)?.price?.toFixed(4) === value.price?.toFixed(4));
		const seriesData = series?.markerDataAtIndex(timePointIndex);

		if (value.price?.toFixed(5) === seriesData?.price?.toFixed(5)) {
			const colors = generateContrastColors(seriesData.backgroundColor);
			commonRendererData.background = colors.background;
			axisRendererData.color = '#fff';

			const additionalPadding = 2 / 12 * this._priceScale.fontSize();

			commonRendererData.additionalPaddingTop = additionalPadding;
			commonRendererData.additionalPaddingBottom = additionalPadding;

			commonRendererData.coordinate = value.coordinate;
			axisRendererData.text = this._priceScale.formatPrice(value.price, firstValue);
			axisRendererData.visible = true;
		} else {
			const colors = generateContrastColors(options.labelBackgroundColor);
			commonRendererData.background = colors.background;
			axisRendererData.color = colors.foreground;

			const additionalPadding = 2 / 12 * this._priceScale.fontSize();

			commonRendererData.additionalPaddingTop = additionalPadding;
			commonRendererData.additionalPaddingBottom = additionalPadding;

			commonRendererData.coordinate = value.coordinate;
			axisRendererData.text = this._priceScale.formatPrice(value.price, firstValue);
			axisRendererData.visible = false;
		}
	}
}
