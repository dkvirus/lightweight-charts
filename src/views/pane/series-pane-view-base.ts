import { ChartModel } from '../../model/chart-model';
import { PriceScale } from '../../model/price-scale';
import { Series } from '../../model/series';
import { SeriesType } from '../../model/series-options';
import { SeriesItemsIndexesRange, TimedValue, visibleTimedValues } from '../../model/time-data';
import { TimeScale } from '../../model/time-scale';
import { IPaneRenderer } from '../../renderers/ipane-renderer';

import { IUpdatablePaneView, UpdateType } from './iupdatable-pane-view';

/**
 * 用于处理图表中的系列数据视图，包括数据的填充、选项的更新、渲染器的准备等
 */
export abstract class SeriesPaneViewBase<TSeriesType extends SeriesType, ItemType extends TimedValue, TRenderer extends IPaneRenderer> implements IUpdatablePaneView {
	// 系列对象
	protected readonly _series: Series<TSeriesType>;
	// 图表模型
	protected readonly _model: ChartModel;
	// 是否需要更新
	protected _invalidated: boolean = true;
	// 数据是否需要更新
	protected _dataInvalidated: boolean = true;
	// 选项是否需要更新
	protected _optionsInvalidated: boolean = true;
	// 时间值数组
	protected _items: ItemType[] = [];
	// 可见的时间值范围 {from: 0, to: 4}
	protected _itemsVisibleRange: SeriesItemsIndexesRange | null = null;
	// 渲染器对象
	protected readonly abstract _renderer: TRenderer;
	// 是否扩展可见范围
	private readonly _extendedVisibleRange: boolean;

	public constructor(series: Series<TSeriesType>, model: ChartModel, extendedVisibleRange: boolean) {
		this._series = series;
		this._model = model;
		this._extendedVisibleRange = extendedVisibleRange;
	}

	/**
	 * 根据传入的更新类型（data或options），设置相应的标志位，表示数据或选项需要更新
	 */
	public update(updateType?: UpdateType): void {
		this._invalidated = true;
		if (updateType === 'data') {
			this._dataInvalidated = true;
		}
		if (updateType === 'options') {
			this._optionsInvalidated = true;
		}
	}

	public renderer(): IPaneRenderer | null {
		// 如果系列不可见，返回null
		if (!this._series.visible()) {
			return null;
		}

		this._makeValid();

		return this._itemsVisibleRange === null ? null : this._renderer;
	}

	/**
	 * 填充原始点
	 *
	 * 更新数据时会调用这个方法
	 */
	protected abstract _fillRawPoints(): void;

	/**
	 * 更新选项，包括应用系列的颜色样式
	 */
	protected _updateOptions(): void {
		this._items = this._items.map((item: ItemType) => ({
			...item,
			...this._series.barColorer().barStyle(item.time),
		}));
	}

	/**
	 * 将数据转换为坐标
	 */
	protected abstract _convertToCoordinates(priceScale: PriceScale, timeScale: TimeScale, firstValue: number): void;

	/**
	 * 清除可见范围
	 */
	protected _clearVisibleRange(): void {
		this._itemsVisibleRange = null;
	}

	/**
	 * 准备渲染器数据
	 */
	protected abstract _prepareRendererData(): void;

	/**
	 * 确保数据、选项和视图都是有效的
	 */
	private _makeValid(): void {
		if (this._dataInvalidated) {
			this._fillRawPoints();
			this._dataInvalidated = false;
		}

		if (this._optionsInvalidated) {
			this._updateOptions();
			this._optionsInvalidated = false;
		}

		if (this._invalidated) {
			this._makeValidImpl();
			this._invalidated = false;
		}
	}

	/**
	 * 实现数据、选项和视图的有效性检查和更新
	 */
	private _makeValidImpl(): void {
		const priceScale = this._series.priceScale();
		const timeScale = this._model.timeScale();

		this._clearVisibleRange();

		// 没有 x 轴或 y 轴配置时直接返回
		if (timeScale.isEmpty() || priceScale.isEmpty()) {
			return;
		}

		// 如果时间轴没有可见范围，直接返回
		const visibleBars = timeScale.visibleStrictRange();
		if (visibleBars === null) {
			return;
		}

		// 如果系列为空，直接返回
		if (this._series.bars().size() === 0) {
			return;
		}

		const firstValue = this._series.firstValue();
		if (firstValue === null) {
			return;
		}

		this._itemsVisibleRange = visibleTimedValues(this._items, visibleBars, this._extendedVisibleRange);
		this._convertToCoordinates(priceScale, timeScale, firstValue.value);

		this._prepareRendererData();
	}
}
