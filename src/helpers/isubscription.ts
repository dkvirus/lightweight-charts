/**
 * Callback 就是个函数类型, 参数类型是动态的, 不固定的
 * 示例: 定义 callback 变量, 类型是 Callback, 很清楚就知道三个参数各是什么类型了
 * `const callback: Callback<number, string, boolean> = (param1, param2, param3) => { }`
 */
export type Callback<T1 = void, T2 = void, T3 = void> = (param1: T1, param2: T2, param3: T3) => void;

export interface ISubscription<T1 = void, T2 = void, T3 = void> {
	subscribe(callback: Callback<T1, T2, T3>, linkedObject?: unknown, singleshot?: boolean): void;
	unsubscribe(callback: Callback<T1, T2, T3>): void;
	unsubscribeAll(linkedObject: unknown): void;
}
