import { Callback, ISubscription } from './isubscription';

interface Listener<T1, T2, T3> {
	callback: Callback<T1, T2, T3>;
	linkedObject?: unknown;
	singleshot: boolean;
}

/**
 * 事件代理
 *
 * Delegate
 * |--_listeners: 事件列表, 列表中每一项都是 `Listener<T1, T2, T3>`
 * |--|-- callback: 回调函数
 * |--|-- linkedObject: 给哪个对象绑定事件监听
 * |--|-- singleshot: 只执行一次的事件函数, 在 fire() 里面触发执行后就把只执行一次的事件给过滤掉了
 * |-- subscribe 添加事件
 * |-- unsubscribe 销毁指定事件
 * |-- unsubscribeAll 销毁指定绑定对象上所有事件
 * |-- fire 执行所有事件
 * |-- hasListeners 判断是否有事件
 * |-- destroy 把所有事件都销毁
 */
export class Delegate<T1 = void, T2 = void, T3 = void> implements ISubscription<T1, T2, T3> {
	private _listeners: Listener<T1, T2, T3>[] = [];

	/**
	 * 订阅
	 * singleshot 没有传参的代表可以执行多次的事件, 不是一次性的
	 */
	public subscribe(callback: Callback<T1, T2, T3>, linkedObject?: unknown, singleshot?: boolean): void {
		const listener: Listener<T1, T2, T3> = {
			callback,
			linkedObject,
			singleshot: singleshot === true,
		};
		this._listeners.push(listener);
	}

	/**
	 * 取消订阅
	 */
	public unsubscribe(callback: Callback<T1, T2, T3>): void {
		// 找到下标, 移除选项, 找 callback 的内存引用
		const index = this._listeners.findIndex((listener: Listener<T1, T2, T3>) => callback === listener.callback);
		if (index > -1) {
			this._listeners.splice(index, 1);
		}
	}

	/**
	 * 取消所有订阅
	 */
	public unsubscribeAll(linkedObject: unknown): void {
		// 找 linkedObject 的内存引用
		this._listeners = this._listeners.filter((listener: Listener<T1, T2, T3>) => listener.linkedObject !== linkedObject);
	}

	/**
	 * 执行事件监听函数
	 */
	public fire(param1: T1, param2: T2, param3: T3): void {
		// subscribe, unsubscribe, unsubscribeAll 这三个方法只会往 _listeners 中添加事件函数, 并不会立刻执行
		const listenersSnapshot = [...this._listeners];
		// 把只执行一次 (listener.singleshot = false) 的事件给过滤掉
		this._listeners = this._listeners.filter((listener: Listener<T1, T2, T3>) => !listener.singleshot);
		// 遍历执行队列里的事件函数
		listenersSnapshot.forEach((listener: Listener<T1, T2, T3>) => listener.callback(param1, param2, param3));
	}

	/**
	 * 判断是否有事件监听函数
	 */
	public hasListeners(): boolean {
		return this._listeners.length > 0;
	}

	/**
	 * 销毁代理, 把所有事件监听函数都清除
	 * 和 unsubscribeAll() 的区别是 unsubscribeAll() 只会清除指定对象下绑定的所有事件
	 * 而 destroy() 是把所有事件都清除
	 */
	public destroy(): void {
		this._listeners = [];
	}
}
