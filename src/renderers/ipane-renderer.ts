import { CanvasRenderingTarget2D } from 'fancy-canvas';

import { HoveredObject } from '../model/chart-model';
import { Coordinate } from '../model/coordinate';

/**
 * 用于描述一个面板渲染器的行为
 */
export interface IPaneRenderer {
	/**
	 * 绘制面板
	 *
	 * target 绘制目标，通常是2D画布上下文
	 * isHovered 一个布尔值，指示面板是否被悬停
	 * hitTestData 一个可选参数，可能包含与命中测试相关的数据
	 */
	draw(target: CanvasRenderingTarget2D, isHovered: boolean, hitTestData?: unknown): void;
	/**
	 * 绘制面板背景
	 *
	 * target 绘制目标，通常是2D画布上下文
	 * isHovered 一个布尔值，指示面板是否被悬停
	 * hitTestData 一个可选参数，可能包含与命中测试相关的数据
	 */
	drawBackground?(target: CanvasRenderingTarget2D, isHovered: boolean, hitTestData?: unknown): void;
	/**
	 * 用于执行命中测试，判断给定的坐标(x, y)是否在面板内。如果坐标在面板内，返回一个HoveredObject对象，否则返回null。
	 */
	hitTest?(x: Coordinate, y: Coordinate): HoveredObject | null;
}
