import { BitmapCoordinatesRenderingScope } from 'fancy-canvas';

import { Coordinate } from '../model/coordinate';
import { SeriesItemsIndexesRange } from '../model/time-data';

import { LinePoint, LineType } from './draw-line';

/**
 * 用于在Canvas上绘制线条
 *
 * renderingScope 包含Canvas的上下文（context）、水平和垂直像素比率（horizontalPixelRatio和verticalPixelRatio）
 * items 包含线条上的点，每个点是一个LinePoint对象
 * lineType 线条类型，可以是简单线条、带步骤的线条或曲线
 * visibleRange 包含可见点的起始和结束索引
 * barWidth 条形宽度
 * styleGetter 一个函数，根据渲染范围和当前点返回线条的样式
 * finishStyledArea 一个函数，在完成样式区域时调用，用于处理样式变化
 */
// eslint-disable-next-line max-params, complexity
export function walkLine<TItem extends LinePoint, TStyle extends CanvasRenderingContext2D['fillStyle'] | CanvasRenderingContext2D['strokeStyle']>(
	renderingScope: BitmapCoordinatesRenderingScope,
	items: readonly TItem[],
	lineType: LineType,
	visibleRange: SeriesItemsIndexesRange,
	barWidth: number,
	// the values returned by styleGetter are compared using the operator !==,
	// so if styleGetter returns objects, then styleGetter should return the same object for equal styles
	styleGetter: (renderingScope: BitmapCoordinatesRenderingScope, item: TItem) => TStyle,
	finishStyledArea: (renderingScope: BitmapCoordinatesRenderingScope, style: TStyle, areaFirstItem: LinePoint, newAreaFirstItem: LinePoint) => void,
	paddingInline: number
): void {
	// 检查items数组是否为空，或者visibleRange是否超出items的范围。如果是，则直接返回
	if (items.length === 0 || visibleRange.from >= items.length || visibleRange.to <= 0) {
		return;
	}

	// 从renderingScope中获取Canvas的上下文（ctx）和像素比率
	const { context: ctx, horizontalPixelRatio, verticalPixelRatio } = renderingScope;

	// 第一个点信息
	const firstItem = items[visibleRange.from];
	let currentStyle = styleGetter(renderingScope, firstItem);
	let currentStyleFirstItem = firstItem;

	// 如果可见范围中的点少于2个，则直接绘制一个条形
	if (visibleRange.to - visibleRange.from < 2) {
		/**
		 * modify by dkvirus at 2024年08月07日15:28:26
		 * 一个点的时候之前是显示一条长线段
		 * 现在改成显示一个很短的线段，看起来就像一个点
		 */
		const radius = 3;

		ctx.beginPath();

		const item1: LinePoint = { x: firstItem.x - radius as Coordinate, y: firstItem.y };
		const item2: LinePoint = { x: firstItem.x + radius as Coordinate, y: firstItem.y };

		ctx.moveTo(item1.x * horizontalPixelRatio, item1.y * verticalPixelRatio);
		ctx.lineTo(item2.x * horizontalPixelRatio, item2.y * verticalPixelRatio);

		finishStyledArea(renderingScope, currentStyle, item1, item2);
	} else {
		// 计算第一个点的坐标
		const firstVisibleIndex = items.findIndex((item: {x: number}) => item.x >= paddingInline);
		let hiddenIndex = firstVisibleIndex;
		if (firstVisibleIndex > 0) {
			hiddenIndex = firstVisibleIndex - 1;
		}

		// 如果可见范围中的点多于2个，则根据lineType绘制不同类型的线条
		const changeStyle = (newStyle: TStyle, currentItem: TItem) => {
			finishStyledArea(renderingScope, currentStyle, currentStyleFirstItem, currentItem);

			ctx.beginPath();
			currentStyle = newStyle;
			currentStyleFirstItem = currentItem;
		};

		let currentItem = currentStyleFirstItem;

		ctx.beginPath();
		if (firstVisibleIndex === hiddenIndex) {
			// 从第一个点的坐标开始绘制
			ctx.moveTo(Math.max(firstItem.x, paddingInline) * horizontalPixelRatio, firstItem.y * verticalPixelRatio);
		} else {
			const { x: x1, y: y1 } = items[firstVisibleIndex];
			const { x: x2, y: y2 } = items[hiddenIndex];
			const tan = (y2 - y1) / (x2 - x1);
			const x3 = paddingInline;
			const y3 = y1 + tan * (x3 - x1);
			ctx.moveTo(x3 * horizontalPixelRatio, y3 * verticalPixelRatio);
		}

		for (let i = visibleRange.from + 1; i < visibleRange.to; ++i) {
			currentItem = items[i];
			if (currentItem.x < paddingInline) {
				continue;
			}
			const itemStyle = styleGetter(renderingScope, currentItem);

			switch (lineType) {
				case LineType.Simple:
					// console.log('currentItem.x', currentItem.x);
					ctx.lineTo(Math.max(currentItem.x, paddingInline) * horizontalPixelRatio, currentItem.y * verticalPixelRatio);
					break;
				case LineType.WithSteps:
					ctx.lineTo(currentItem.x * horizontalPixelRatio, items[i - 1].y * verticalPixelRatio);

					if (itemStyle !== currentStyle) {
						changeStyle(itemStyle, currentItem);
						ctx.lineTo(currentItem.x * horizontalPixelRatio, items[i - 1].y * verticalPixelRatio);
					}

					ctx.lineTo(currentItem.x * horizontalPixelRatio, currentItem.y * verticalPixelRatio);
					break;
				case LineType.Curved: {
					const [cp1, cp2] = getControlPoints(items, i - 1, i);
					ctx.bezierCurveTo(
						cp1.x * horizontalPixelRatio,
						cp1.y * verticalPixelRatio,
						cp2.x * horizontalPixelRatio,
						cp2.y * verticalPixelRatio,
						currentItem.x * horizontalPixelRatio,
						currentItem.y * verticalPixelRatio
					);
					break;
				}
			}

			if (lineType !== LineType.WithSteps && itemStyle !== currentStyle) {
				changeStyle(itemStyle, currentItem);
				ctx.moveTo(currentItem.x * horizontalPixelRatio, currentItem.y * verticalPixelRatio);
			}
		}

		if (currentStyleFirstItem !== currentItem || currentStyleFirstItem === currentItem && lineType === LineType.WithSteps) {
			finishStyledArea(renderingScope, currentStyle, currentStyleFirstItem, currentItem);
		}
	}
}

const curveTension = 6;

function subtract(p1: LinePoint, p2: LinePoint): LinePoint {
	return { x: p1.x - p2.x as Coordinate, y: p1.y - p2.y as Coordinate };
}

function add(p1: LinePoint, p2: LinePoint): LinePoint {
	return { x: p1.x + p2.x as Coordinate, y: p1.y + p2.y as Coordinate };
}

function divide(p1: LinePoint, n: number): LinePoint {
	return { x: p1.x / n as Coordinate, y: p1.y / n as Coordinate };
}

/**
 * @returns Two control points that can be used as arguments to {@link CanvasRenderingContext2D.bezierCurveTo} to draw a curved line between `points[fromPointIndex]` and `points[toPointIndex]`.
 */
export function getControlPoints(points: readonly LinePoint[], fromPointIndex: number, toPointIndex: number): [LinePoint, LinePoint] {
	const beforeFromPointIndex = Math.max(0, fromPointIndex - 1);
	const afterToPointIndex = Math.min(points.length - 1, toPointIndex + 1);
	const cp1 = add(points[fromPointIndex], divide(subtract(points[toPointIndex], points[beforeFromPointIndex]), curveTension));
	const cp2 = subtract(points[toPointIndex], divide(subtract(points[afterToPointIndex], points[fromPointIndex]), curveTension));

	return [cp1, cp2];
}
