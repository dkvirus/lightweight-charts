import { BitmapCoordinatesRenderingScope } from 'fancy-canvas';

import { PricedValue } from '../model/price-scale';
import { SeriesItemsIndexesRange, TimedValue } from '../model/time-data';

import { BitmapCoordinatesPaneRenderer } from './bitmap-coordinates-pane-renderer';
import { LinePoint, LineStyle, LineType, LineWidth, setLineStyle } from './draw-line';
import { drawSeriesPointMarkers } from './draw-series-point-markers';
import { walkLine } from './walk-line';

export type LineItemBase = TimedValue & PricedValue & LinePoint;

export interface PaneRendererLineDataBase<TItem extends LineItemBase = LineItemBase> {
	lineType?: LineType;

	items: TItem[];

	barWidth: number;
	paddingInline: number;

	lineWidth: LineWidth;
	lineStyle: LineStyle;

	visibleRange: SeriesItemsIndexesRange | null;

	pointMarkersRadius?: number;
}

function finishStyledArea(scope: BitmapCoordinatesRenderingScope, style: CanvasRenderingContext2D['strokeStyle']): void {
	const ctx = scope.context;
	ctx.strokeStyle = style;
	ctx.stroke();
}

export abstract class PaneRendererLineBase<TData extends PaneRendererLineDataBase> extends BitmapCoordinatesPaneRenderer {
	protected _data: TData | null = null;

	public setData(data: TData): void {
		this._data = data;
	}

	/**
	 * 用于在给定的渲染范围内绘制图表或图形
	 */
	protected _drawImpl(renderingScope: BitmapCoordinatesRenderingScope): void {
		// 没有数据，直接返回
		if (this._data === null) {
			return;
		}

		const { items, visibleRange, barWidth, paddingInline, lineType, lineWidth, lineStyle, pointMarkersRadius } = this._data;

		// x 轴没有可见范围，直接返回
		if (visibleRange === null) {
			return;
		}

		// 获取 canvas 上下文，也可以认为是获取画笔
		const ctx = renderingScope.context;

		// 设置线段两端样式, butt 是横平竖直那一种
		ctx.lineCap = 'butt';
		// 设置线宽
		ctx.lineWidth = lineWidth * renderingScope.verticalPixelRatio;

		// 设置线段是虚线还是实线
		setLineStyle(ctx, lineStyle);

		// 设置线段连接处样式
		ctx.lineJoin = 'round';

		// 线段颜色
		const styleGetter = this._strokeStyle.bind(this);

		if (lineType !== undefined) {
			// 画线段
			walkLine(renderingScope, items, lineType, visibleRange, barWidth, styleGetter, finishStyledArea, paddingInline);
		}

		if (pointMarkersRadius) {
			drawSeriesPointMarkers(renderingScope, items, pointMarkersRadius, visibleRange, styleGetter);
		}
	}

	protected abstract _strokeStyle(renderingScope: BitmapCoordinatesRenderingScope, item: TData['items'][0]): CanvasRenderingContext2D['strokeStyle'];
}
