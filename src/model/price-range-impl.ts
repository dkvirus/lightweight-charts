import { isNumber } from '../helpers/strict-type-checks';

import { PriceScaleOptions } from './price-scale';
import { PriceRange } from './series-options';

function computeFiniteResult(
	method: (...values: number[]) => number,
	valueOne: number,
	valueTwo: number,
	fallback: number
): number {
	const firstFinite = Number.isFinite(valueOne);
	const secondFinite = Number.isFinite(valueTwo);

	if (firstFinite && secondFinite) {
		return method(valueOne, valueTwo);
	}

	return !firstFinite && !secondFinite ? fallback : (firstFinite ? valueOne : valueTwo);
}

export class PriceRangeImpl {
	private _minValue: number;
	private _maxValue!: number;

	public constructor(minValue: number, maxValue: number) {
		this._minValue = minValue;
		this._maxValue = maxValue;
	}

	public equals(pr: PriceRangeImpl | null): boolean {
		if (pr === null) {
			return false;
		}
		return this._minValue === pr._minValue && this._maxValue === pr._maxValue;
	}

	public clone(): PriceRangeImpl {
		return new PriceRangeImpl(this._minValue, this._maxValue);
	}

	public minValue(): number {
		return this._minValue;
	}

	public maxValue(): number {
		return this._maxValue;
	}

	public setMinValue(minValue: number): void {
		this._minValue = minValue;
	}

	public setMaxValue(maxValue: number): void {
		this._maxValue = maxValue;
	}

	public length(): number {
		return this._maxValue - this._minValue;
	}

	/**
	 * 最大值等于最小值
	 */
	public isEmpty(): boolean {
		return this._maxValue === this._minValue || Number.isNaN(this._maxValue) || Number.isNaN(this._minValue);
	}

	public merge(anotherRange: PriceRangeImpl | null): PriceRangeImpl {
		if (anotherRange === null) {
			return this;
		}
		return new PriceRangeImpl(
			computeFiniteResult(Math.min, this.minValue(), anotherRange.minValue(), -Infinity),
			computeFiniteResult(Math.max, this.maxValue(), anotherRange.maxValue(), Infinity)
		);
	}

	public scaleAroundCenter(coeff: number, options: PriceScaleOptions): void {
		if (!isNumber(coeff)) {
			return;
		}

		const delta = this._maxValue - this._minValue;
		if (delta === 0) {
			return;
		}

		const center = (this._maxValue + this._minValue) * 0.5;
		let maxDelta = this._maxValue - center;
		let minDelta = this._minValue - center;
		maxDelta *= coeff;
		minDelta *= coeff;

		/**
		 * modify by dkvirus at 2023年10月25日19:51:59
		 * 设置 y 轴最小值为 0
		 */
		let maxValue = center + maxDelta;
		let minValue = center + minDelta;
		if (minValue < 0) {
			minValue = 0;
			maxValue = maxValue - minValue;
		}

		/**
		 * https://gitlab.com/dkvirus/lightweight-charts/-/issues/1
		 */
		if (
			options.maxValue !== undefined &&
			maxValue >= options.maxValue * (1 - options?.scaleMargins?.top)
		) {
			this._maxValue = options.maxValue * (1 - options?.scaleMargins?.top);
		} else {
			this._maxValue = maxValue;
		}

		if (options.fixBottomEdge) {
			this._minValue = 0;
		} else {
			this._minValue = minValue;
		}
	}

	public shift(delta: number): void {
		if (!isNumber(delta)) {
			return;
		}
		this._maxValue = this._maxValue + delta;
		this._minValue = this._minValue + delta;
	}

	public toRaw(): PriceRange {
		return {
			minValue: this._minValue,
			maxValue: this._maxValue,
		};
	}

	public static fromRaw(raw: PriceRange | null): PriceRangeImpl | null {
		return (raw === null) ? null : new PriceRangeImpl(raw.minValue, raw.maxValue);
	}
}
