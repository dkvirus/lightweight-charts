import { assert, ensureDefined, ensureNotNull } from '../helpers/assertions';
import { Delegate } from '../helpers/delegate';
import { IDestroyable } from '../helpers/idestroyable';
import { ISubscription } from '../helpers/isubscription';
import { clone, DeepPartial } from '../helpers/strict-type-checks';

import { ChartModel, ChartOptions, OverlayPriceScaleOptions, VisiblePriceScaleOptions } from './chart-model';
import { DefaultPriceScaleId, isDefaultPriceScale } from './default-price-scale';
import { Grid } from './grid';
import { IPriceDataSource } from './iprice-data-source';
import { PriceScale, PriceScaleOptions, PriceScaleState } from './price-scale';
import { sortSources } from './sort-sources';
import { TimeScale } from './time-scale';

export const DEFAULT_STRETCH_FACTOR = 1000;

export type PriceScalePosition = 'left' | 'right' | 'overlay';

interface MinMaxOrderInfo {
	minZOrder: number;
	maxZOrder: number;
}

export class Pane implements IDestroyable {
	// x轴对象
	private readonly _timeScale: TimeScale;
	// 图表模型，包含图表的配置和状态
	private readonly _model: ChartModel;
	// 网格对象，用于绘制图表的网格线
	private readonly _grid: Grid;

	// 数据源数组，存储所有添加到面板中的数据源
	private _dataSources: IPriceDataSource[] = [];
	// 一个映射，用于存储每个价格比例尺对应的数据源数组
	private _overlaySourcesByScaleId: Map<string, IPriceDataSource[]> = new Map();

	// 面板的高度
	private _height: number = 0;
	// 面板的宽度
	private _width: number = 0;
	// 拉伸因子，用于调整面板的大小
	private _stretchFactor: number = DEFAULT_STRETCH_FACTOR;
	// 缓存的数据源数组，按特定顺序排列
	private _cachedOrderedSources: readonly IPriceDataSource[] | null = null;

	// 一个委托对象，用于处理销毁事件
	private _destroyed: Delegate = new Delegate();

	// 左侧的y轴对象
	private _leftPriceScale: PriceScale;
	// 右侧的y轴对象
	private _rightPriceScale: PriceScale;

	public constructor(timeScale: TimeScale, model: ChartModel) {
		this._timeScale = timeScale;
		this._model = model;
		// 创建网格对象
		this._grid = new Grid(this);

		const options = model.options();

		// 初始化左右两个y轴对象
		this._leftPriceScale = this._createPriceScale(DefaultPriceScaleId.Left, options.leftPriceScale);
		this._rightPriceScale = this._createPriceScale(DefaultPriceScaleId.Right, options.rightPriceScale);

		// 订阅左右两个y轴状态变化
		this._leftPriceScale.modeChanged().subscribe(this._onPriceScaleModeChanged.bind(this, this._leftPriceScale), this);
		this._rightPriceScale.modeChanged().subscribe(this._onPriceScaleModeChanged.bind(this, this._rightPriceScale), this);

		this.applyScaleOptions(options);
	}

	/**
	 * 应用新的 y 轴配置
	 */
	public applyScaleOptions(options: DeepPartial<ChartOptions>): void {
		if (options.leftPriceScale) {
			this._leftPriceScale.applyOptions(options.leftPriceScale);
		}
		if (options.rightPriceScale) {
			this._rightPriceScale.applyOptions(options.rightPriceScale);
		}
		if (options.localization) {
			this._leftPriceScale.updateFormatter();
			this._rightPriceScale.updateFormatter();
		}
		if (options.overlayPriceScales) {
			const sourceArrays = Array.from(this._overlaySourcesByScaleId.values());
			for (const arr of sourceArrays) {
				const priceScale = ensureNotNull(arr[0].priceScale());
				priceScale.applyOptions(options.overlayPriceScales);
				if (options.localization) {
					priceScale.updateFormatter();
				}
			}
		}
	}

	/**
	 * 根据 id 获取指定 y 轴
	 */
	public priceScaleById(id: string): PriceScale | null {
		switch (id) {
			case DefaultPriceScaleId.Left: {
				return this._leftPriceScale;
			}
			case DefaultPriceScaleId.Right: {
				return this._rightPriceScale;
			}
		}
		if (this._overlaySourcesByScaleId.has(id)) {
			return ensureDefined(this._overlaySourcesByScaleId.get(id))[0].priceScale();
		}
		return null;
	}

	/**
	 * 销毁面板，包括取消订阅、销毁数据源等
	 */
	public destroy(): void {
		this.model().priceScalesOptionsChanged().unsubscribeAll(this);

		this._leftPriceScale.modeChanged().unsubscribeAll(this);
		this._rightPriceScale.modeChanged().unsubscribeAll(this);

		this._dataSources.forEach((source: IPriceDataSource) => {
			if (source.destroy) {
				source.destroy();
			}
		});
		this._destroyed.fire();
	}

	/**
	 * 获取拉伸因子
	 */
	public stretchFactor(): number {
		return this._stretchFactor;
	}

	/**
	 * 设置拉伸因子
	 */
	public setStretchFactor(factor: number): void {
		this._stretchFactor = factor;
	}

	/**
	 * 获取图表模型
	 */
	public model(): ChartModel {
		return this._model;
	}

	/**
	 * 获取面板宽度
	 */
	public width(): number {
		return this._width;
	}

	/**
	 * 获取面板高度
	 */
	public height(): number {
		return this._height;
	}

	/**
	 * 设置面板宽度
	 */
	public setWidth(width: number): void {
		this._width = width;
		this.updateAllSources();
	}

	/**
	 * 设置面板高度
	 */
	public setHeight(height: number): void {
		this._height = height;

		this._leftPriceScale.setHeight(height);
		this._rightPriceScale.setHeight(height);

		// process overlays
		this._dataSources.forEach((ds: IPriceDataSource) => {
			if (this.isOverlay(ds)) {
				const priceScale = ds.priceScale();
				if (priceScale !== null) {
					priceScale.setHeight(height);
				}
			}
		});

		this.updateAllSources();
	}

	/**
	 * 获取数据源数组
	 */
	public dataSources(): readonly IPriceDataSource[] {
		return this._dataSources;
	}

	/**
	 * 判断数据源是否是覆盖类型?? 自定义面板?
	 */
	public isOverlay(source: IPriceDataSource): boolean {
		const priceScale = source.priceScale();
		if (priceScale === null) {
			return true;
		}
		return this._leftPriceScale !== priceScale && this._rightPriceScale !== priceScale;
	}

	/**
	 * 添加数据源到面板
	 */
	public addDataSource(source: IPriceDataSource, targetScaleId: string, zOrder?: number): void {
		const targetZOrder = (zOrder !== undefined) ? zOrder : this._getZOrderMinMax().maxZOrder + 1;
		this._insertDataSource(source, targetScaleId, targetZOrder);
	}

	/**
	 * 从面板中移除数据源
	 */
	public removeDataSource(source: IPriceDataSource): void {
		const index = this._dataSources.indexOf(source);
		assert(index !== -1, 'removeDataSource: invalid data source');

		this._dataSources.splice(index, 1);

		const priceScaleId = ensureNotNull(source.priceScale()).id();
		if (this._overlaySourcesByScaleId.has(priceScaleId)) {
			const overlaySources = ensureDefined(this._overlaySourcesByScaleId.get(priceScaleId));
			const overlayIndex = overlaySources.indexOf(source);
			if (overlayIndex !== -1) {
				overlaySources.splice(overlayIndex, 1);
				if (overlaySources.length === 0) {
					this._overlaySourcesByScaleId.delete(priceScaleId);
				}
			}
		}

		const priceScale = source.priceScale();
		// if source has owner, it returns owner's price scale
		// and it does not have source in their list
		if (priceScale && priceScale.dataSources().indexOf(source) >= 0) {
			priceScale.removeDataSource(source);
		}

		if (priceScale !== null) {
			priceScale.invalidateSourcesCache();
			this.recalculatePriceScale(priceScale);
		}

		this._cachedOrderedSources = null;
	}

	/**
	 * 获取 y 轴位置
	 *
	 * 返回 left 或者 right 或者 overlay
	 */
	public priceScalePosition(priceScale: PriceScale): PriceScalePosition {
		if (priceScale === this._leftPriceScale) {
			return 'left';
		}
		if (priceScale === this._rightPriceScale) {
			return 'right';
		}

		return 'overlay';
	}

	/**
	 * 获取左边 y 轴对象
	 */
	public leftPriceScale(): PriceScale {
		return this._leftPriceScale;
	}

	/**
	 * 获取右边 y 轴对象
	 */
	public rightPriceScale(): PriceScale {
		return this._rightPriceScale;
	}

	/* **************************** y 轴缩放 START ******************************* */

	public startScalePrice(priceScale: PriceScale, x: number): void {
		priceScale.startScale(x);
	}

	public scalePriceTo(priceScale: PriceScale, x: number): void {
		priceScale.scaleTo(x);

		// TODO: be more smart and update only affected views
		this.updateAllSources();
	}

	public endScalePrice(priceScale: PriceScale): void {
		priceScale.endScale();
	}

	/* **************************** y 轴缩放 END ******************************* */
	/* **************************** y 轴滚动 START ******************************* */

	public startScrollPrice(priceScale: PriceScale, x: number): void {
		priceScale.startScroll(x);
	}

	public scrollPriceTo(priceScale: PriceScale, x: number): void {
		priceScale.scrollTo(x);
		this.updateAllSources();
	}

	public endScrollPrice(priceScale: PriceScale): void {
		priceScale.endScroll();
	}

	/* **************************** y 轴滚动 END ******************************* */

	/**
	 * 更新所有数据源
	 */
	public updateAllSources(): void {
		this._dataSources.forEach((source: IPriceDataSource) => {
			source.updateAllViews();
		});
	}

	/**
	 * 获取默认 y 轴对象
	 */
	public defaultPriceScale(): PriceScale {
		let priceScale: PriceScale | null = null;

		if (this._model.options().rightPriceScale.visible && this._rightPriceScale.dataSources().length !== 0) {
			priceScale = this._rightPriceScale;
		} else if (this._model.options().leftPriceScale.visible && this._leftPriceScale.dataSources().length !== 0) {
			priceScale = this._leftPriceScale;
		} else if (this._dataSources.length !== 0) {
			priceScale = this._dataSources[0].priceScale();
		}

		if (priceScale === null) {
			priceScale = this._rightPriceScale;
		}

		return priceScale;
	}

	/**
	 * 获取所有 y 轴对象
	 */
	public getPriceScales(): PriceScale[] {
		const priceScales: PriceScale[] = [];

		if (this._model.options().rightPriceScale.visible && this._rightPriceScale.dataSources().length !== 0) {
			priceScales.push(this._rightPriceScale);
		}
		if (this._model.options().leftPriceScale.visible && this._leftPriceScale.dataSources().length !== 0) {
			priceScales.push(this._leftPriceScale);
		}

		if (!priceScales?.length) {
			priceScales.push(this._rightPriceScale);
		}

		return priceScales;
	}

	/**
	 * 获取默认可见的 y 轴对象
	 */
	public defaultVisiblePriceScale(): PriceScale | null {
		let priceScale: PriceScale | null = null;

		if (this._model.options().rightPriceScale.visible) {
			priceScale = this._rightPriceScale;
		} else if (this._model.options().leftPriceScale.visible) {
			priceScale = this._leftPriceScale;
		}
		return priceScale;
	}

	/**
	 * 重新计算 y 轴对象
	 */
	public recalculatePriceScale(priceScale: PriceScale | null): void {
		if (priceScale === null || !priceScale.isAutoScale()) {
			return;
		}

		this._recalculatePriceScaleImpl(priceScale);
	}

	/**
	 * 重置 y 轴对象
	 */
	public resetPriceScale(priceScale: PriceScale): void {
		const visibleBars = this._timeScale.visibleStrictRange();
		priceScale.setMode({ autoScale: true });
		if (visibleBars !== null) {
			priceScale.recalculatePriceRange(visibleBars);
		}
		this.updateAllSources();
	}

	/**
	 * 临时自动调整 y 轴对象
	 */
	public momentaryAutoScale(): void {
		this._recalculatePriceScaleImpl(this._leftPriceScale);
		this._recalculatePriceScaleImpl(this._rightPriceScale);
	}

	/**
	 * 重新计算所有 y 轴对象
	 */
	public recalculate(): void {
		this.recalculatePriceScale(this._leftPriceScale);
		this.recalculatePriceScale(this._rightPriceScale);

		this._dataSources.forEach((ds: IPriceDataSource) => {
			if (this.isOverlay(ds)) {
				this.recalculatePriceScale(ds.priceScale());
			}
		});

		this.updateAllSources();
		this._model.lightUpdate();
	}

	/**
	 * 获取排序后的数据源数组
	 */
	public orderedSources(): readonly IPriceDataSource[] {
		if (this._cachedOrderedSources === null) {
			this._cachedOrderedSources = sortSources(this._dataSources);
		}

		return this._cachedOrderedSources;
	}

	/**
	 * 获取销毁事件的订阅
	 */
	public onDestroyed(): ISubscription {
		return this._destroyed;
	}

	/**
	 * 获取网格对象
	 */
	public grid(): Grid {
		return this._grid;
	}

	/**
	 * 重新计算特定价格比例尺
	 */
	private _recalculatePriceScaleImpl(priceScale: PriceScale): void {
		// TODO: can use this checks
		const sourceForAutoScale = priceScale.sourcesForAutoScale();

		if (sourceForAutoScale && sourceForAutoScale.length > 0 && !this._timeScale.isEmpty()) {
			const visibleBars = this._timeScale.visibleStrictRange();
			if (visibleBars !== null) {
				priceScale.recalculatePriceRange(visibleBars);
			}
		}

		priceScale.updateAllViews();
	}

	/**
	 * 获取数据源的最小和最大Z顺序
	 */
	private _getZOrderMinMax(): MinMaxOrderInfo {
		const sources = this.orderedSources();
		if (sources.length === 0) {
			return { minZOrder: 0, maxZOrder: 0 };
		}

		let minZOrder = 0;
		let maxZOrder = 0;
		for (let j = 0; j < sources.length; j++) {
			const ds = sources[j];
			const zOrder = ds.zorder();
			if (zOrder !== null) {
				if (zOrder < minZOrder) {
					minZOrder = zOrder;
				}

				if (zOrder > maxZOrder) {
					maxZOrder = zOrder;
				}
			}
		}

		return { minZOrder: minZOrder, maxZOrder: maxZOrder };
	}

	/**
	 * 插入数据源到面板
	 */
	private _insertDataSource(source: IPriceDataSource, priceScaleId: string, zOrder: number): void {
		let priceScale = this.priceScaleById(priceScaleId);

		if (priceScale === null) {
			priceScale = this._createPriceScale(priceScaleId, this._model.options().overlayPriceScales);
		}

		this._dataSources.push(source);
		if (!isDefaultPriceScale(priceScaleId)) {
			const overlaySources = this._overlaySourcesByScaleId.get(priceScaleId) || [];
			overlaySources.push(source);
			this._overlaySourcesByScaleId.set(priceScaleId, overlaySources);
		}

		priceScale.addDataSource(source);
		source.setPriceScale(priceScale);

		source.setZorder(zOrder);

		this.recalculatePriceScale(priceScale);

		this._cachedOrderedSources = null;
	}

	/**
	 * 处理价格比例尺模式变化
	 */
	private _onPriceScaleModeChanged(priceScale: PriceScale, oldMode: PriceScaleState, newMode: PriceScaleState): void {
		if (oldMode.mode === newMode.mode) {
			return;
		}

		// momentary auto scale if we toggle percentage/indexedTo100 mode
		this._recalculatePriceScaleImpl(priceScale);
	}

	/**
	 * 创建新的价格比例尺
	 */
	private _createPriceScale(id: string, options: OverlayPriceScaleOptions | VisiblePriceScaleOptions): PriceScale {
		const actualOptions: PriceScaleOptions = { visible: true, autoScale: true, ...clone(options) };
		const priceScale = new PriceScale(
			id,
			actualOptions,
			this._model.options().layout,
			this._model.options().localization,
			this._model
		);
		priceScale.setHeight(this.height());
		return priceScale;
	}
}
