import { lowerbound, upperbound } from '../helpers/algorithms';
import { ensureNotNull } from '../helpers/assertions';
import { Nominal } from '../helpers/nominal';

import { PlotRow, PlotRowValueIndex } from '../model/plot-data';
import { TimePointIndex } from '../model/time-data';

/**
 * Search direction if no data found at provided index
 */
export const enum MismatchDirection {
	/**
	 * Search the nearest left item
	 */
	NearestLeft = -1,
	/**
	 * Do not search
	 */
	None = 0,
	/**
	 * Search the nearest right item
	 */
	NearestRight = 1,
}

export interface MinMax {
	min: number;
	max: number;
}

type PlotRowIndex = Nominal<number, 'PlotRowIndex'>;

// TODO: think about changing it dynamically
const CHUNK_SIZE = 30;

/**
 * 这个类的主要用途是管理和操作一系列的绘图行数据，支持基本的增删改查操作，并提供了缓存机制来优化性能。
 */
export class PlotList<PlotRowType extends PlotRow = PlotRow> {
	// 一个只读数组，存储了所有的绘图行数据
	private _items: readonly PlotRowType[] = [];
	// 一个 Map，用于缓存特定范围内的最小值和最大值
	private _minMaxCache: Map<PlotRowValueIndex, Map<number, MinMax | null>> = new Map();
	// 一个 Map，用于缓存特定时间点索引的搜索结果
	private _rowSearchCache: Map<TimePointIndex, Map<MismatchDirection, PlotRowType>> = new Map();

	/**
	 * 返回列表中的最后一行数据，如果没有数据则返回 null
	 */
	// @returns Last row
	public last(): PlotRowType | null {
		return this.size() > 0 ? this._items[this._items.length - 1] : null;
	}

	/**
	 * 返回列表中第一行的索引，如果没有数据则返回 null
	 */
	public firstIndex(): TimePointIndex | null {
		return this.size() > 0 ? this._indexAt(0 as PlotRowIndex) : null;
	}

	/**
	 * 返回列表中最后一行的索引，如果没有数据则返回 null
	 */
	public lastIndex(): TimePointIndex | null {
		return this.size() > 0 ? this._indexAt((this._items.length - 1) as PlotRowIndex) : null;
	}

	/**
	 * 返回列表中绘图行的数量
	 */
	public size(): number {
		return this._items.length;
	}

	/**
	 * 判断列表是否为空
	 */
	public isEmpty(): boolean {
		return this.size() === 0;
	}

	/**
	 * 判断列表中是否包含指定索引的数据
	 */
	public contains(index: TimePointIndex): boolean {
		return this._search(index, MismatchDirection.None) !== null;
	}

	/**
	 * 返回指定索引的数据
	 */
	public valueAt(index: TimePointIndex): PlotRowType | null {
		return this.search(index);
	}

	/**
	 * 根据指定的搜索模式，返回最接近指定索引的数据
	 */
	public search(index: TimePointIndex, searchMode: MismatchDirection = MismatchDirection.None): PlotRowType | null {
		const pos = this._search(index, searchMode);
		if (pos === null) {
			return null;
		}

		return {
			...this._valueAt(pos),
			index: this._indexAt(pos),
		};
	}

	/**
	 * 返回所有绘图行的只读数组
	 */
	public rows(): readonly PlotRowType[] {
		return this._items;
	}

	/**
	 * 返回指定范围内绘图行的最小值和最大值
	 */
	public minMaxOnRangeCached(start: TimePointIndex, end: TimePointIndex, plots: readonly PlotRowValueIndex[]): MinMax | null {
		// this code works for single series only
		// could fail after whitespaces implementation

		if (this.isEmpty()) {
			return null;
		}

		let result: MinMax | null = null;

		for (const plot of plots) {
			const plotMinMax = this._minMaxOnRangeCachedImpl(start, end, plot);
			result = mergeMinMax(result, plotMinMax);
		}

		return result;
	}

	/**
	 * 设置新的绘图行数据，并清空缓存
	 */
	public setData(plotRows: readonly PlotRowType[]): void {
		this._rowSearchCache.clear();
		this._minMaxCache.clear();

		this._items = plotRows;
	}

	/**
	 * 返回指定偏移量的索引
	 */
	private _indexAt(offset: PlotRowIndex): TimePointIndex {
		return this._items[offset].index;
	}

	/**
	 * 返回指定偏移量的绘图行数据
	 */
	private _valueAt(offset: PlotRowIndex): PlotRowType {
		return this._items[offset];
	}

	/**
	 * 根据搜索模式，返回最接近指定索引的绘图行数据
	 */
	private _search(index: TimePointIndex, searchMode: MismatchDirection): PlotRowIndex | null {
		const exactPos = this._bsearch(index);

		if (exactPos === null && searchMode !== MismatchDirection.None) {
			switch (searchMode) {
				case MismatchDirection.NearestLeft:
					return this._searchNearestLeft(index);
				case MismatchDirection.NearestRight:
					return this._searchNearestRight(index);
				default:
					throw new TypeError('Unknown search mode');
			}
		}

		return exactPos;
	}

	/**
	 * 返回最接近指定索引的左侧绘图行数据
	 */
	private _searchNearestLeft(index: TimePointIndex): PlotRowIndex | null {
		let nearestLeftPos = this._lowerbound(index);
		if (nearestLeftPos > 0) {
			nearestLeftPos = nearestLeftPos - 1;
		}

		return (nearestLeftPos !== this._items.length && this._indexAt(nearestLeftPos as PlotRowIndex) < index) ? nearestLeftPos as PlotRowIndex : null;
	}

	/**
	 * 返回最接近指定索引的右侧绘图行数据
	 */
	private _searchNearestRight(index: TimePointIndex): PlotRowIndex | null {
		const nearestRightPos = this._upperbound(index);
		return (nearestRightPos !== this._items.length && index < this._indexAt(nearestRightPos as PlotRowIndex)) ? nearestRightPos as PlotRowIndex : null;
	}

	/**
	 * 使用二分查找返回最接近指定索引的绘图行数据
	 */
	private _bsearch(index: TimePointIndex): PlotRowIndex | null {
		const start = this._lowerbound(index);
		if (start !== this._items.length && !(index < this._items[start as PlotRowIndex].index)) {
			return start as PlotRowIndex;
		}

		return null;
	}

	/**
	 * 返回大于或等于指定索引的最小索引
	 */
	private _lowerbound(index: TimePointIndex): number {
		return lowerbound(
			this._items,
			index,
			(a: PlotRowType, b: TimePointIndex) => a.index < b
		);
	}

	/**
	 * 返回小于指定索引的最大索引
	 */
	private _upperbound(index: TimePointIndex): number {
		return upperbound(
			this._items,
			index,
			(a: TimePointIndex, b: PlotRowType) => b.index > a
		);
	}

	/**
	 * 计算指定范围内绘图行的最小值和最大值
	 */
	private _plotMinMax(startIndex: PlotRowIndex, endIndexExclusive: PlotRowIndex, plotIndex: PlotRowValueIndex): MinMax | null {
		let result: MinMax | null = null;

		for (let i = startIndex; i < endIndexExclusive; i++) {
			const values = this._items[i].value;

			const v = values[plotIndex];
			if (Number.isNaN(v)) {
				continue;
			}

			if (result === null) {
				result = { min: v, max: v };
			} else {
				if (v < result.min) {
					result.min = v;
				}

				if (v > result.max) {
					result.max = v;
				}
			}
		}

		return result;
	}

	/**
	 * 计算指定范围内绘图行的最小值和最大值，并使用缓存优化性能
	 */
	private _minMaxOnRangeCachedImpl(start: TimePointIndex, end: TimePointIndex, plotIndex: PlotRowValueIndex): MinMax | null {
		// this code works for single series only
		// could fail after whitespaces implementation

		if (this.isEmpty()) {
			return null;
		}

		let result: MinMax | null = null;

		// assume that bar indexes only increase
		const firstIndex = ensureNotNull(this.firstIndex());
		const lastIndex = ensureNotNull(this.lastIndex());

		const s = Math.max(start, firstIndex);
		const e = Math.min(end, lastIndex);

		const cachedLow = Math.ceil(s / CHUNK_SIZE) * CHUNK_SIZE;
		const cachedHigh = Math.max(cachedLow, Math.floor(e / CHUNK_SIZE) * CHUNK_SIZE);

		{
			const startIndex = this._lowerbound(s as TimePointIndex);
			const endIndex = this._upperbound(Math.min(e, cachedLow, end) as TimePointIndex); // non-inclusive end
			const plotMinMax = this._plotMinMax(startIndex as PlotRowIndex, endIndex as PlotRowIndex, plotIndex);
			result = mergeMinMax(result, plotMinMax);
		}

		let minMaxCache = this._minMaxCache.get(plotIndex);

		if (minMaxCache === undefined) {
			minMaxCache = new Map();
			this._minMaxCache.set(plotIndex, minMaxCache);
		}

		// now go cached
		for (let c = Math.max(cachedLow + 1, s); c < cachedHigh; c += CHUNK_SIZE) {
			const chunkIndex = Math.floor(c / CHUNK_SIZE);

			let chunkMinMax = minMaxCache.get(chunkIndex);
			if (chunkMinMax === undefined) {
				const chunkStart = this._lowerbound(chunkIndex * CHUNK_SIZE as TimePointIndex);
				const chunkEnd = this._upperbound((chunkIndex + 1) * CHUNK_SIZE - 1 as TimePointIndex);
				chunkMinMax = this._plotMinMax(chunkStart as PlotRowIndex, chunkEnd as PlotRowIndex, plotIndex);
				minMaxCache.set(chunkIndex, chunkMinMax);
			}

			result = mergeMinMax(result, chunkMinMax);
		}

		// tail
		{
			const startIndex = this._lowerbound(cachedHigh as TimePointIndex);
			const endIndex = this._upperbound(e as TimePointIndex); // non-inclusive end
			const plotMinMax = this._plotMinMax(startIndex as PlotRowIndex, endIndex as PlotRowIndex, plotIndex);
			result = mergeMinMax(result, plotMinMax);
		}

		return result;
	}
}

function mergeMinMax(first: MinMax | null, second: MinMax | null): MinMax | null {
	if (first === null) {
		return second;
	} else {
		if (second === null) {
			return first;
		} else {
			// merge MinMax values
			const min = Math.min(first.min, second.min);
			const max = Math.max(first.max, second.max);
			return { min: min, max: max };
		}
	}
}
