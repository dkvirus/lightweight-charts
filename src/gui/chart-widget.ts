import { Size, size } from 'fancy-canvas';

import { ensureDefined, ensureNotNull } from '../helpers/assertions';
import { isChromiumBased, isWindows } from '../helpers/browsers';
import { Delegate } from '../helpers/delegate';
import { IDestroyable } from '../helpers/idestroyable';
import { ISubscription } from '../helpers/isubscription';
import { warn } from '../helpers/logger';
import { DeepPartial } from '../helpers/strict-type-checks';

import { ChartModel, ChartOptionsInternal } from '../model/chart-model';
import { Coordinate } from '../model/coordinate';
import { DefaultPriceScaleId } from '../model/default-price-scale';
import {
	InvalidateMask,
	InvalidationLevel,
	TimeScaleInvalidation,
	TimeScaleInvalidationType,
} from '../model/invalidate-mask';
import { Point } from '../model/point';
import { Series } from '../model/series';
import { SeriesPlotRow } from '../model/series-data';
import { OriginalTime, TimePointIndex } from '../model/time-data';
import { TouchMouseEventData } from '../model/touch-mouse-event-data';

import { suggestChartSize, suggestPriceScaleWidth, suggestTimeScaleHeight } from './internal-layout-sizes-hints';
import { PaneWidget } from './pane-widget';
import { TimeAxisWidget } from './time-axis-widget';

export interface MouseEventParamsImpl {
	time?: OriginalTime;
	index?: TimePointIndex;
	point?: Point;
	seriesData: Map<Series, SeriesPlotRow>;
	hoveredSeries?: Series;
	hoveredObject?: string;
	touchMouseEventData?: TouchMouseEventData;
}

export type MouseEventParamsImplSupplier = () => MouseEventParamsImpl;

const windowsChrome = isChromiumBased() && isWindows();

export class ChartWidget implements IDestroyable {
	private readonly _options: ChartOptionsInternal;
	private _paneWidgets: PaneWidget[] = [];
	private readonly _model: ChartModel;
	private _drawRafId: number = 0;
	private _height: number = 0;
	private _width: number = 0;
	private _leftPriceAxisWidth: number = 0;
	private _rightPriceAxisWidth: number = 0;
	private _element: HTMLDivElement;
	private readonly _tableElement: HTMLElement;
	private _timeAxisWidget: TimeAxisWidget;
	private _invalidateMask: InvalidateMask | null = null;
	private _drawPlanned: boolean = false;
	private _clicked: Delegate<MouseEventParamsImplSupplier> = new Delegate();
	private _dblClicked: Delegate<MouseEventParamsImplSupplier> = new Delegate();
	private _crosshairMoved: Delegate<MouseEventParamsImplSupplier> = new Delegate();
	private _onWheelBound: (event: WheelEvent) => void;
	private _observer: ResizeObserver | null = null;

	private _container: HTMLElement;
	private _cursorStyleOverride: string | null = null;

	/**
	 * 初始化图表的各个部分，包括模型、面板、时间轴等
	 *
	 * 图表控件由多个面板（PaneWidget）和时间轴（TimeAxisWidget）组成。
	 * 每个面板可以显示不同的数据系列，时间轴则显示时间刻度。
	 */
	public constructor(container: HTMLElement, options: ChartOptionsInternal) {
		// 图表外层容器对象
		this._container = container;
		// 图表参数
		this._options = options;

		/**
		 * div#container
		 * |--div.tv-lightweight-charts     _element
		 * |---- table     _tableElement
		 * |------ tr
		 * |------ tr
		 */

		/**
		 * 线图外层容器元素
		 *
		 * q: 页面上 style 有 user-select 和 -webkit-tap-highlight-color 在哪里设置的不知道?
		 * a: 下面的 disableSelection() 方法里面添加了这两个属性, 作用是禁止选中文字
		 *
		 * q: 页面上 style 的 width 和 height 是具体数值, 这里是 100%, 也对不上
		 */
		this._element = document.createElement('div');
		this._element.classList.add('tv-lightweight-charts');
		this._element.style.overflow = 'hidden';
		this._element.style.direction = 'ltr';
		this._element.style.width = '100%';
		this._element.style.height = '100%';
		// 禁止选中文字
		disableSelection(this._element);

		// 创建 table 元素并添加到 _element 元素里面
		this._tableElement = document.createElement('table');
		this._tableElement.setAttribute('cellspacing', '0');
		this._element.appendChild(this._tableElement);

		/**
		 * 等同于将函数 this._onMousewheel 复制一份, 命名为 this._onWheelBound
		 * 当调用 this._onWheelBound() 的时候, 里面的 this 指向的就是当前的 ChartWidget
		 *
		 * 鼠标中间的滚轮事件, 向上滚动时放大图形, 向下滚动时缩小事件
		 */
		this._onWheelBound = this._onMousewheel.bind(this);
		/**
		 * options.handleScroll.mouseWheel, options.handleScale.mouseWheel 这两个配置只要有一个为 true
		 * 就添加滚轮监听事件, 两个都为 false, 不监听滚轮事件, 鼠标滚轮无效
		 *
		 * q: scroll 滚轮和 scale 滚轮应该要分开弄, 页面表现是不一样的
		 * a: 滚轮事件是统一绑定在 this._element 元素上的, 并没有细致到绑定到 y 轴和线图面板上
		 */
		if (shouldSubscribeMouseWheel(this._options)) {
			// 添加监听滚轮事件
			this._setMouseWheelEventListener(true);
		}
		/**
		 * 注册 model
		 *
		 * q: 什么是 model
		 * a: 处理业务逻辑的相关代码?
		 *
		 * _invalidateHandler() 在 ChartWidget 中定义, 在 ChartModel 中调用
		 */
		this._model = new ChartModel(
			this._invalidateHandler.bind(this),
			this._options
		);
		/**
		 * crosshairMoved() 订阅事件, 只是添加到事件列表中, 还未执行
		 * _onPaneWidgetCrosshairMoved 中的 this 指 ChartWidget
		 */
		this.model().crosshairMoved().subscribe(this._onPaneWidgetCrosshairMoved.bind(this), this);

		this._timeAxisWidget = new TimeAxisWidget(this);
		this._tableElement.appendChild(this._timeAxisWidget.getElement());

		const usedObserver = options.autoSize && this._installObserver();

		// observer could not fire event immediately for some cases
		// so we have to set initial size manually
		let width = this._options.width;
		let height = this._options.height;
		// ignore width/height options if observer has actually been used
		// however respect options if installing resize observer failed
		if (usedObserver || width === 0 || height === 0) {
			const containerRect = container.getBoundingClientRect();
			width = width || containerRect.width;
			height = height || containerRect.height;
		}

		// BEWARE: resize must be called BEFORE _syncGuiWithModel (in constructor only)
		// or after but with adjustSize to properly update time scale
		this.resize(width, height);

		this._syncGuiWithModel();

		container.appendChild(this._element);
		this._updateTimeAxisVisibility();
		this._model.timeScale().optionsApplied().subscribe(this._model.fullUpdate.bind(this._model), this);
		this._model.priceScalesOptionsChanged().subscribe(this._model.fullUpdate.bind(this._model), this);
	}

	/**
	 * 获取图表模型对象
	 */
	public model(): ChartModel {
		return this._model;
	}

	/**
	 * 获取图表的配置选项
	 */
	public options(): Readonly<ChartOptionsInternal> {
		return this._options;
	}

	/**
	 * 获取图表的面板控件数组
	 */
	public paneWidgets(): PaneWidget[] {
		return this._paneWidgets;
	}

	/**
	 * 获取时间轴控件
	 */
	public timeAxisWidget(): TimeAxisWidget {
		return this._timeAxisWidget;
	}

	/**
	 * 销毁图表控件，清理所有资源
	 */
	public destroy(): void {
		this._setMouseWheelEventListener(false);
		if (this._drawRafId !== 0) {
			window.cancelAnimationFrame(this._drawRafId);
		}

		this._model.crosshairMoved().unsubscribeAll(this);
		this._model.timeScale().optionsApplied().unsubscribeAll(this);
		this._model.priceScalesOptionsChanged().unsubscribeAll(this);
		this._model.destroy();

		for (const paneWidget of this._paneWidgets) {
			this._tableElement.removeChild(paneWidget.getElement());
			paneWidget.clicked().unsubscribeAll(this);
			paneWidget.dblClicked().unsubscribeAll(this);
			paneWidget.destroy();
		}
		this._paneWidgets = [];

		ensureNotNull(this._timeAxisWidget).destroy();

		if (this._element.parentElement !== null) {
			this._element.parentElement.removeChild(this._element);
		}

		this._crosshairMoved.destroy();
		this._clicked.destroy();
		this._dblClicked.destroy();

		this._uninstallObserver();
	}

	/**
	 * 调整图表控件的大小
	 */
	public resize(width: number, height: number, forceRepaint: boolean = false): void {
		if (this._height === height && this._width === width) {
			return;
		}

		const sizeHint = suggestChartSize(size({ width, height }));

		this._height = sizeHint.height;
		this._width = sizeHint.width;

		const heightStr = this._height + 'px';
		const widthStr = this._width + 'px';

		ensureNotNull(this._element).style.height = heightStr;
		ensureNotNull(this._element).style.width = widthStr;

		this._tableElement.style.height = heightStr;
		this._tableElement.style.width = widthStr;

		if (forceRepaint) {
			this._drawImpl(InvalidateMask.full(), performance.now());
		} else {
			this._model.fullUpdate();
		}
	}

	/**
	 * 绘制图表
	 */
	public paint(invalidateMask?: InvalidateMask): void {
		if (invalidateMask === undefined) {
			invalidateMask = InvalidateMask.full();
		}

		for (let i = 0; i < this._paneWidgets.length; i++) {
			this._paneWidgets[i].paint(invalidateMask.invalidateForPane(i).level);
		}

		if (this._options.timeScale.visible) {
			this._timeAxisWidget.paint(invalidateMask.fullInvalidation());
		}
	}

	/**
	 * 应用新的配置选项
	 */
	public applyOptions(options: DeepPartial<ChartOptionsInternal>): void {
		const currentlyHasMouseWheelListener = shouldSubscribeMouseWheel(this._options);

		// we don't need to merge options here because it's done in chart model
		// and since both model and widget share the same object it will be done automatically for widget as well
		// not ideal solution for sure, but it work's for now ¯\_(ツ)_/¯
		this._model.applyOptions(options);

		const shouldHaveMouseWheelListener = shouldSubscribeMouseWheel(this._options);
		if (shouldHaveMouseWheelListener !== currentlyHasMouseWheelListener) {
			this._setMouseWheelEventListener(shouldHaveMouseWheelListener);
		}

		this._updateTimeAxisVisibility();

		this._applyAutoSizeOptions(options);
	}

	/**
	 * 获取鼠标事件订阅对象
	 */
	public clicked(): ISubscription<MouseEventParamsImplSupplier> {
		return this._clicked;
	}

	public dblClicked(): ISubscription<MouseEventParamsImplSupplier> {
		return this._dblClicked;
	}

	public crosshairMoved(): ISubscription<MouseEventParamsImplSupplier> {
		return this._crosshairMoved;
	}

	/**
	 * 获取图表的截图
	 */
	public takeScreenshot(): HTMLCanvasElement {
		if (this._invalidateMask !== null) {
			this._drawImpl(this._invalidateMask, performance.now());
			this._invalidateMask = null;
		}

		const screeshotBitmapSize = this._traverseLayout(null);
		const screenshotCanvas = document.createElement('canvas');
		screenshotCanvas.width = screeshotBitmapSize.width;
		screenshotCanvas.height = screeshotBitmapSize.height;

		const ctx = ensureNotNull(screenshotCanvas.getContext('2d'));
		this._traverseLayout(ctx);

		return screenshotCanvas;
	}

	/**
	 * 获取价格轴的宽度
	 */
	public getPriceAxisWidth(position: DefaultPriceScaleId): number {
		if (position === 'left' && !this._isLeftAxisVisible()) {
			return 0;
		}

		if (position === 'right' && !this._isRightAxisVisible()) {
			return 0;
		}

		if (this._paneWidgets.length === 0) {
			return 0;
		}

		// we don't need to worry about exactly pane widget here
		// because all pane widgets have the same width of price axis widget
		// see _adjustSizeImpl
		const priceAxisWidget = position === 'left'
			? this._paneWidgets[0].leftPriceAxisWidget()
			: this._paneWidgets[0].rightPriceAxisWidget();
		return ensureNotNull(priceAxisWidget).getWidth();
	}

	/**
	 * 检查是否启用了自动调整大小功能
	 */
	public autoSizeActive(): boolean {
		return this._options.autoSize && this._observer !== null;
	}

	/**
	 * 获取图表控件的 HTML 元素
	 */
	public element(): HTMLDivElement {
		return this._element;
	}

	/**
	 * 设置鼠标光标样式
	 */
	public setCursorStyle(style: string | null): void {
		this._cursorStyleOverride = style;
		if (this._cursorStyleOverride) {
			this.element().style.setProperty('cursor', style);
		} else {
			this.element().style.removeProperty('cursor');
		}
	}

	/**
	 * 获取当前鼠标光标样式
	 */
	public getCursorOverrideStyle(): string | null {
		return this._cursorStyleOverride;
	}

	/**
	 * 应用自动调整大小选项
	 */
	// eslint-disable-next-line complexity
	private _applyAutoSizeOptions(options: DeepPartial<ChartOptionsInternal>): void {
		if (options.autoSize === undefined && this._observer && (options.width !== undefined || options.height !== undefined)) {
			warn(`You should turn autoSize off explicitly before specifying sizes; try adding options.autoSize: false to new options`);
			return;
		}
		if (options.autoSize && !this._observer) {
			// installing observer will override resize if successful
			this._installObserver();
		}

		if (options.autoSize === false && this._observer !== null) {
			this._uninstallObserver();
		}

		if (!options.autoSize && (options.width !== undefined || options.height !== undefined)) {
			this.resize(options.width || this._width, options.height || this._height);
		}
	}

	/**
	 * 遍历布局并绘制截图
	 */
	private _traverseLayout(ctx: CanvasRenderingContext2D | null): Size {
		let totalWidth = 0;
		let totalHeight = 0;

		const firstPane = this._paneWidgets[0];

		const drawPriceAxises = (position: 'left' | 'right', targetX: number) => {
			let targetY = 0;
			for (let paneIndex = 0; paneIndex < this._paneWidgets.length; paneIndex++) {
				const paneWidget = this._paneWidgets[paneIndex];
				const priceAxisWidget = ensureNotNull(position === 'left' ? paneWidget.leftPriceAxisWidget() : paneWidget.rightPriceAxisWidget());
				const bitmapSize = priceAxisWidget.getBitmapSize();
				if (ctx !== null) {
					priceAxisWidget.drawBitmap(ctx, targetX, targetY);
				}
				targetY += bitmapSize.height;
			}
		};

		// draw left price scale if exists
		if (this._isLeftAxisVisible()) {
			drawPriceAxises('left', 0);
			const leftAxisBitmapWidth = ensureNotNull(firstPane.leftPriceAxisWidget()).getBitmapSize().width;
			totalWidth += leftAxisBitmapWidth;
		}
		for (let paneIndex = 0; paneIndex < this._paneWidgets.length; paneIndex++) {
			const paneWidget = this._paneWidgets[paneIndex];
			const bitmapSize = paneWidget.getBitmapSize();
			if (ctx !== null) {
				paneWidget.drawBitmap(ctx, totalWidth, totalHeight);
			}
			totalHeight += bitmapSize.height;
		}
		const firstPaneBitmapWidth = firstPane.getBitmapSize().width;
		totalWidth += firstPaneBitmapWidth;

		// draw right price scale if exists
		if (this._isRightAxisVisible()) {
			drawPriceAxises('right', totalWidth);
			const rightAxisBitmapWidth = ensureNotNull(firstPane.rightPriceAxisWidget()).getBitmapSize().width;
			totalWidth += rightAxisBitmapWidth;
		}

		const drawStub = (position: 'left' | 'right', targetX: number, targetY: number) => {
			const stub = ensureNotNull(position === 'left' ? this._timeAxisWidget.leftStub() : this._timeAxisWidget.rightStub());
			stub.drawBitmap(ensureNotNull(ctx), targetX, targetY);
		};

		// draw time scale and stubs
		if (this._options.timeScale.visible) {
			const timeAxisBitmapSize = this._timeAxisWidget.getBitmapSize();

			if (ctx !== null) {
				let targetX = 0;
				if (this._isLeftAxisVisible()) {
					drawStub('left', targetX, totalHeight);
					targetX = ensureNotNull(firstPane.leftPriceAxisWidget()).getBitmapSize().width;
				}

				this._timeAxisWidget.drawBitmap(ctx, targetX, totalHeight);
				targetX += timeAxisBitmapSize.width;

				if (this._isRightAxisVisible()) {
					drawStub('right', targetX, totalHeight);
				}
			}

			totalHeight += timeAxisBitmapSize.height;
		}

		return size({
			width: totalWidth,
			height: totalHeight,
		});
	}

	/**
	 * 调整图表控件的大小
	 */
	// eslint-disable-next-line complexity
	private _adjustSizeImpl(): void {
		let totalStretch = 0;
		let leftPriceAxisWidth = 0;
		let rightPriceAxisWidth = 0;

		for (const paneWidget of this._paneWidgets) {
			if (this._isLeftAxisVisible()) {
				leftPriceAxisWidth = Math.max(leftPriceAxisWidth, ensureNotNull(paneWidget.leftPriceAxisWidget()).optimalWidth());
			}
			if (this._isRightAxisVisible()) {
				rightPriceAxisWidth = Math.max(rightPriceAxisWidth, ensureNotNull(paneWidget.rightPriceAxisWidget()).optimalWidth());
			}
			totalStretch += paneWidget.stretchFactor();
		}

		leftPriceAxisWidth = suggestPriceScaleWidth(leftPriceAxisWidth);
		rightPriceAxisWidth = suggestPriceScaleWidth(rightPriceAxisWidth);

		const width = this._width;
		const height = this._height;

		const paneWidth = Math.max(width - leftPriceAxisWidth - rightPriceAxisWidth, 0);

		const separatorsHeight = 0; // separatorHeight * separatorCount;
		const timeAxisVisible = this._options.timeScale.visible;
		let timeAxisHeight = timeAxisVisible ? this._timeAxisWidget.optimalHeight() : 0;
		timeAxisHeight = suggestTimeScaleHeight(timeAxisHeight);

		const otherWidgetHeight = separatorsHeight + timeAxisHeight;
		const totalPaneHeight = height < otherWidgetHeight ? 0 : height - otherWidgetHeight;
		const stretchPixels = totalPaneHeight / totalStretch;

		let accumulatedHeight = 0;
		for (let paneIndex = 0; paneIndex < this._paneWidgets.length; ++paneIndex) {
			const paneWidget = this._paneWidgets[paneIndex];
			paneWidget.setState(this._model.panes()[paneIndex]);

			let paneHeight = 0;
			let calculatePaneHeight = 0;

			if (paneIndex === this._paneWidgets.length - 1) {
				calculatePaneHeight = totalPaneHeight - accumulatedHeight;
			} else {
				calculatePaneHeight = Math.round(paneWidget.stretchFactor() * stretchPixels);
			}

			paneHeight = Math.max(calculatePaneHeight, 2);

			accumulatedHeight += paneHeight;

			paneWidget.setSize(size({ width: paneWidth, height: paneHeight }));
			if (this._isLeftAxisVisible()) {
				paneWidget.setPriceAxisSize(leftPriceAxisWidth, 'left');
			}
			if (this._isRightAxisVisible()) {
				paneWidget.setPriceAxisSize(rightPriceAxisWidth, 'right');
			}

			if (paneWidget.state()) {
				this._model.setPaneHeight(paneWidget.state(), paneHeight);
			}
		}

		this._timeAxisWidget.setSizes(
			size({ width: timeAxisVisible ? paneWidth : 0, height: timeAxisHeight }),
			timeAxisVisible ? leftPriceAxisWidth : 0,
			timeAxisVisible ? rightPriceAxisWidth : 0
		);

		this._model.setWidth(paneWidth);
		if (this._leftPriceAxisWidth !== leftPriceAxisWidth) {
			this._leftPriceAxisWidth = leftPriceAxisWidth;
		}
		if (this._rightPriceAxisWidth !== rightPriceAxisWidth) {
			this._rightPriceAxisWidth = rightPriceAxisWidth;
		}
	}

	/**
	 * 添加或移除鼠标滚轮事件监听器
	 * 滚轮事件是绑定在 this._element 元素上的
	 */
	private _setMouseWheelEventListener(add: boolean): void {
		if (add) {
			this._element.addEventListener('wheel', this._onWheelBound, { passive: false });
			return;
		}
		this._element.removeEventListener('wheel', this._onWheelBound);
	}

	private _determineWheelSpeedAdjustment(event: WheelEvent): number {
		switch (event.deltaMode) {
			case event.DOM_DELTA_PAGE:
				// one screen at time scroll mode
				return 120;
			case event.DOM_DELTA_LINE:
				// one line at time scroll mode
				return 32;
		}

		if (!windowsChrome) {
			return 1;
		}

		// Chromium on Windows has a bug where the scroll speed isn't correctly
		// adjusted for high density displays. We need to correct for this so that
		// scroll speed is consistent between browsers.
		// https://bugs.chromium.org/p/chromium/issues/detail?id=1001735
		// https://bugs.chromium.org/p/chromium/issues/detail?id=1207308
		return (1 / window.devicePixelRatio);
	}

	/**
	 * 处理鼠标滚轮事件
	 */
	private _onMousewheel(event: WheelEvent): void {
		if ((event.deltaX === 0 || !this._options.handleScroll.mouseWheel) &&
			(event.deltaY === 0 || !this._options.handleScale.mouseWheel)) {
			return;
		}

		const scrollSpeedAdjustment = this._determineWheelSpeedAdjustment(event);

		const deltaX = scrollSpeedAdjustment * event.deltaX / 100;
		const deltaY = -(scrollSpeedAdjustment * event.deltaY / 100);

		if (event.cancelable) {
			event.preventDefault();
		}

		if (deltaY !== 0 && this._options.handleScale.mouseWheel) {
			const zoomScale = Math.sign(deltaY) * Math.min(1, Math.abs(deltaY));
			const scrollPosition = event.clientX - this._element.getBoundingClientRect().left;
			this.model().zoomTime(scrollPosition as Coordinate, zoomScale);
		}

		if (deltaX !== 0 && this._options.handleScroll.mouseWheel) {
			this.model().scrollChart(deltaX * -80 as Coordinate); // 80 is a made up coefficient, and minus is for the "natural" scroll
		}
	}

	/**
	 * 绘制图表的实现
	 */
	private _drawImpl(invalidateMask: InvalidateMask, time: number): void {
		const invalidationType = invalidateMask.fullInvalidation();

		// actions for full invalidation ONLY (not shared with light)
		if (invalidationType === InvalidationLevel.Full) {
			this._updateGui();
		}

		// light or full invalidate actions
		if (
			invalidationType === InvalidationLevel.Full ||
			invalidationType === InvalidationLevel.Light
		) {
			this._applyMomentaryAutoScale(invalidateMask);
			this._applyTimeScaleInvalidations(invalidateMask, time);

			this._timeAxisWidget.update();
			this._paneWidgets.forEach((pane: PaneWidget) => {
				pane.updatePriceAxisWidgets();
			});

			// In the case a full invalidation has been postponed during the draw, reapply
			// the timescale invalidations. A full invalidation would mean there is a change
			// in the timescale width (caused by price scale changes) that needs to be drawn
			// right away to avoid flickering.
			if (this._invalidateMask?.fullInvalidation() === InvalidationLevel.Full) {
				this._invalidateMask.merge(invalidateMask);

				this._updateGui();

				this._applyMomentaryAutoScale(this._invalidateMask);
				this._applyTimeScaleInvalidations(this._invalidateMask, time);

				invalidateMask = this._invalidateMask;
				this._invalidateMask = null;
			}
		}

		this.paint(invalidateMask);
	}

	/**
	 * 应用时间轴的无效化
	 */
	private _applyTimeScaleInvalidations(invalidateMask: InvalidateMask, time: number): void {
		for (const tsInvalidation of invalidateMask.timeScaleInvalidations()) {
			this._applyTimeScaleInvalidation(tsInvalidation, time);
		}
	}

	/**
	 * 应用临时的自动缩放
	 */
	private _applyMomentaryAutoScale(invalidateMask: InvalidateMask): void {
		const panes = this._model.panes();
		for (let i = 0; i < panes.length; i++) {
			if (invalidateMask.invalidateForPane(i).autoScale) {
				panes[i].momentaryAutoScale();
			}
		}
	}

	/**
	 * 应用时间轴的无效化
	 */
	private _applyTimeScaleInvalidation(invalidation: TimeScaleInvalidation, time: number): void {
		const timeScale = this._model.timeScale();
		switch (invalidation.type) {
			case TimeScaleInvalidationType.FitContent:
				timeScale.fitContent();
				break;
			case TimeScaleInvalidationType.ApplyRange:
				timeScale.setLogicalRange(invalidation.value);
				break;
			case TimeScaleInvalidationType.ApplyBarSpacing:
				timeScale.setBarSpacing(invalidation.value);
				break;
			case TimeScaleInvalidationType.ApplyRightOffset:
				timeScale.setRightOffset(invalidation.value);
				break;
			case TimeScaleInvalidationType.Reset:
				timeScale.restoreDefault();
				break;
			case TimeScaleInvalidationType.Animation:
				if (!invalidation.value.finished(time)) {
					timeScale.setRightOffset(invalidation.value.getPosition(time));
				}
				break;
		}
	}

	/**
	 * 图表模型调用此方法来通知图表控件需要重新绘制
	 * _invalidateHandler() 在 ChartWidget 中定义, 在 ChartModel 中调用
	 */
	private _invalidateHandler(invalidateMask: InvalidateMask): void {
		if (this._invalidateMask !== null) {
			this._invalidateMask.merge(invalidateMask);
		} else {
			this._invalidateMask = invalidateMask;
		}

		if (!this._drawPlanned) {
			this._drawPlanned = true;
			this._drawRafId = window.requestAnimationFrame((time: number) => {
				this._drawPlanned = false;
				this._drawRafId = 0;

				if (this._invalidateMask !== null) {
					const mask = this._invalidateMask;
					this._invalidateMask = null;
					this._drawImpl(mask, time);

					for (const tsInvalidation of mask.timeScaleInvalidations()) {
						if (tsInvalidation.type === TimeScaleInvalidationType.Animation && !tsInvalidation.value.finished(time)) {
							this.model().setTimeScaleAnimation(tsInvalidation.value);
							break;
						}
					}
				}
			});
		}
	}

	/**
	 * 更新图表的界面
	 */
	private _updateGui(): void {
		this._syncGuiWithModel();
	}

	/**
	 * 同步图表控件的状态与模型
	 */
	private _syncGuiWithModel(): void {
		const panes = this._model.panes();
		const targetPaneWidgetsCount = panes.length;
		const actualPaneWidgetsCount = this._paneWidgets.length;

		// Remove (if needed) pane widgets and separators
		for (let i = targetPaneWidgetsCount; i < actualPaneWidgetsCount; i++) {
			const paneWidget = ensureDefined(this._paneWidgets.pop());
			this._tableElement.removeChild(paneWidget.getElement());
			paneWidget.clicked().unsubscribeAll(this);
			paneWidget.dblClicked().unsubscribeAll(this);
			paneWidget.destroy();
		}

		// Create (if needed) new pane widgets and separators
		for (let i = actualPaneWidgetsCount; i < targetPaneWidgetsCount; i++) {
			const paneWidget = new PaneWidget(this, panes[i]);
			paneWidget.clicked().subscribe(this._onPaneWidgetClicked.bind(this), this);
			paneWidget.dblClicked().subscribe(this._onPaneWidgetDblClicked.bind(this), this);

			this._paneWidgets.push(paneWidget);

			// insert paneWidget
			this._tableElement.insertBefore(paneWidget.getElement(), this._timeAxisWidget.getElement());
		}

		for (let i = 0; i < targetPaneWidgetsCount; i++) {
			const state = panes[i];
			const paneWidget = this._paneWidgets[i];
			if (paneWidget.state() !== state) {
				paneWidget.setState(state);
			} else {
				paneWidget.updatePriceAxisWidgetsStates();
			}
		}

		this._updateTimeAxisVisibility();
		this._adjustSizeImpl();
	}

	/**
	 * 获取鼠标事件参数的实现
	 */
	private _getMouseEventParamsImpl(
		index: TimePointIndex | null,
		point: Point | null,
		event: TouchMouseEventData | null
	): MouseEventParamsImpl {
		const seriesData = new Map<Series, SeriesPlotRow>();
		if (index !== null) {
			const serieses = this._model.serieses();
			serieses.forEach((s: Series) => {
				// TODO: replace with search left
				const data = s.bars().search(index);
				if (data !== null) {
					seriesData.set(s, data);
				}
			});
		}
		let clientTime: OriginalTime | undefined;
		if (index !== null) {
			const timePoint = this._model.timeScale().indexToTimeScalePoint(index)?.originalTime;
			if (timePoint !== undefined) {
				clientTime = timePoint;
			}
		}

		const hoveredSource = this.model().hoveredSource();

		const hoveredSeries = hoveredSource !== null && hoveredSource.source instanceof Series
			? hoveredSource.source
			: undefined;

		const hoveredObject = hoveredSource !== null && hoveredSource.object !== undefined
			? hoveredSource.object.externalId
			: undefined;

		return {
			time: clientTime,
			index: index ?? undefined,
			point: point ?? undefined,
			hoveredSeries,
			seriesData,
			hoveredObject,
			touchMouseEventData: event ?? undefined,
		};
	}

	/**
	 * 处理面板控件的点击、双击和十字光标移动事件
	 */
	private _onPaneWidgetClicked(
		time: TimePointIndex | null,
		point: Point | null,
		event: TouchMouseEventData
	): void {
		this._clicked.fire(() => this._getMouseEventParamsImpl(time, point, event));
	}

	private _onPaneWidgetDblClicked(
		time: TimePointIndex | null,
		point: Point | null,
		event: TouchMouseEventData
	): void {
		this._dblClicked.fire(() => this._getMouseEventParamsImpl(time, point, event));
	}

	/**
	 * 在 ChartModel 中执行 _crosshairMoved.fire() 时才会调用 _onPaneWidgetCrosshairMoved 方法
	 * 这个方法的三个参数也是 ChartModel 中执行 _crosshairMoved.fire() 传递的
	 */
	private _onPaneWidgetCrosshairMoved(
		time: TimePointIndex | null,
		point: Point | null,
		event: TouchMouseEventData | null
	): void {
		this._crosshairMoved.fire(() => this._getMouseEventParamsImpl(time, point, event));
	}

	/**
	 * 更新时间轴的可见性
	 */
	private _updateTimeAxisVisibility(): void {
		const display = this._options.timeScale.visible ? '' : 'none';
		this._timeAxisWidget.getElement().style.display = display;
	}

	/**
	 * 检查左价格轴是否可见
	 */
	private _isLeftAxisVisible(): boolean {
		return this._paneWidgets[0].state().leftPriceScale().options().visible;
	}

	/**
	 * 检查右价格轴是否可见
	 */
	private _isRightAxisVisible(): boolean {
		return this._paneWidgets[0].state().rightPriceScale().options().visible;
	}

	/**
	 * 安装一个观察器，用于监听容器大小变化
	 */
	private _installObserver(): boolean {
		// eslint-disable-next-line no-restricted-syntax
		if (!('ResizeObserver' in window)) {
			warn('Options contains "autoSize" flag, but the browser does not support ResizeObserver feature. Please provide polyfill.');
			return false;
		} else {
			this._observer = new ResizeObserver((entries: ResizeObserverEntry[]) => {
				const containerEntry = entries.find((entry: ResizeObserverEntry) => entry.target === this._container);
				if (!containerEntry) {
					return;
				}
				this.resize(containerEntry.contentRect.width, containerEntry.contentRect.height);
			});
			this._observer.observe(this._container, { box: 'border-box' });
			return true;
		}
	}

	/**
	 * 卸载观察器
	 */
	private _uninstallObserver(): void {
		if (this._observer !== null) {
			this._observer.disconnect();
		}
		this._observer = null;
	}
}

/**
 * 通过 js 处理元素的 css user-select 属性, 禁止选中文字
 */
function disableSelection(element: HTMLElement): void {
	element.style.userSelect = 'none';
	// eslint-disable-next-line deprecation/deprecation
	element.style.webkitUserSelect = 'none';
	// eslint-disable-next-line @typescript-eslint/no-explicit-any,@typescript-eslint/no-unsafe-member-access
	(element.style as any).msUserSelect = 'none';
	// eslint-disable-next-line @typescript-eslint/no-explicit-any,@typescript-eslint/no-unsafe-member-access
	(element.style as any).MozUserSelect = 'none';

	// eslint-disable-next-line @typescript-eslint/no-explicit-any,@typescript-eslint/no-unsafe-member-access
	(element.style as any).webkitTapHighlightColor = 'transparent';
}

/**
 * 判断是否需要订阅鼠标滚轮事件
 */
function shouldSubscribeMouseWheel(options: ChartOptionsInternal): boolean {
	return Boolean(options.handleScroll.mouseWheel || options.handleScale.mouseWheel);
}
